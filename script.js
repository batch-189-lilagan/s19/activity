// Activity 1 - User Input
let username, password, role;

function userLogin() {

    // Get user input
    username = prompt(`Enter your username:`);
    password = prompt(`Enter your password:`);
    role = prompt(`Enter your role:`);

    // Check if empty - I added ' ' to prevent accepting blank space as input
    username = username === '' || username === ' ' || username === null || username === undefined ? 'empty' : username ;
    password = password === '' || password === ' ' || password === null || password === undefined ? 'empty' : password;
    role = role === '' || role === ' ' || role === null || role === undefined ? 'empty' : role;

    // If one of the inputs is 'empty'...
    if (username === 'empty' || role === 'empty' || role === 'empty') {
        alert('⚠ All of your inputs must not be empty!');
    }
    
    // If all input is valid...
    else {

        // To accurately match the given conditions...
        role = role.toLowerCase();

        switch(role) {

            case 'admin':
                alert('👩🏻‍💻 Welcome back to the class portal, admin!');
                break;

            case 'teacher':
                alert('👨🏻‍🏫 Thank you for logging in, teacher!');
                break;

            case 'rookie':
                alert('👨🏻‍🎓 Welcome to the class portal, student');
                break;

            default:
                alert('⚠ Role out of range');
        }

    }

}

// Invoke the function...
userLogin();


// Activity 2 - Grade Calculator
function checkAverage(grade1, grade2, grade3, grade4) {

    let average = Math.round((grade1 + grade2 + grade3 + grade4) / 4);
    
    if (average <= 74) {
        console.log(`Hello, student, your average is ${average}. The letter equivalent is F.`);
    }

    else if (average >= 75 && average <= 79) {
        console.log(`Hello, student, your average is ${average}. The letter equivalent is D.`);
    }

    else if (average >= 80 && average <= 84) {
        console.log(`Hello, student, your average is ${average}. The letter equivalent is C.`);
    }

    else if (average >= 85 && average <= 89) {
        console.log(`Hello, student, your average is ${average}. The letter equivalent is B.`);
    }

    else if (average >= 90 && average <= 95) {
        console.log(`Hello, student, your average is ${average}. The letter equivalent is A.`);
    }

    else if (average >= 96) {
        console.log(`Hello, student, your average is ${average}. The letter equivalent is A+`);
    }

}

// Invoke the function...
checkAverage(90,85,92,82);